package colourfilter;

import java.util.function.*;
import javafx.application.*;
import javafx.scene.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.*;
import javafx.stage.*;
//comment
public class ColourFilter2 extends Application {
 
    public static Image transform(Image in, UnaryOperator<Color> f) {
      int width = (int) in.getWidth();
      int height = (int) in.getHeight();
      WritableImage out = new WritableImage(
         width, height);
      for (int x = 0; x < width; x++)
         for (int y = 0; y < height; y++)
            out.getPixelWriter().setColor(x, y, 
               f.apply(in.getPixelReader().getColor(x, y)));
      return out;
   }
    
    
   public static <T> UnaryOperator<T> compose(UnaryOperator<T> op1,
      UnaryOperator<T> op2) {
      return t -> op2.apply(op1.apply(t));
   }
  
   //@FunctionalInterface
    public interface ColourTransformer{
        Color apply(int x, int y, Color colorAtXY);

        public Color apply(Color color);
    }
   
   public static Image transform(Image in, ColourTransformer f)
   {
       int width = (int) in.getWidth();
      int height = (int) in.getHeight();
      WritableImage out = new WritableImage(
         width, height);
      for (int x = 0; x < width; x++)
         for (int y = 0; y < height; y++)
            out.getPixelWriter().setColor(x, y, 
               f.apply(in.getPixelReader().getColor(x, y)));
      return out;
   }
   
   public void start(Stage stage) {
      Image image = new Image("amazing-trees.jpg");
       //Image image2 = transform(image, Color::brighter);
       //Image image3 = transform(image2, Color::grayscale);
      // alternative to two previous image transforms -- composition
      Image image3 = transform(image, compose(Color::brighter, Color::grayscale));  
      stage.setScene(new Scene(new VBox(
         new ImageView(image),
         // new ImageView(image2),
         new ImageView(image3))));
      //Image image4 = transform(image, compose());
      
      stage.show();
   }
}
