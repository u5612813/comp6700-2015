package ass2;

/**
 * Created with IntelliJ IDEA.
 * User: abx
 * Date: 27/04/2015
 * Time: 11:50 AM
 * Created for ass2 in package ass2
 * @author abx
 * @author (your name and id)
 * @see ass2.crawlreader.CrawlReader
 */

import ass2.crawlreader.CrawlReader;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

  import javafx.scene.control.*;
  import javafx.scene.layout.*;

import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Arc;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.PathTransition.OrientationType;
import javafx.animation.Timeline;
import javafx.scene.text.Text;

import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Math.random;
import java.util.Properties;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.geometry.Pos;
import javafx.util.Duration;
//import javafx.scene.shape.Path;

public class StarWarsCrawler extends Application {

    private Group root;
    private CrawlReader model;
    
    //static boolean startTransition=false;
    
    static final double shrinkFactorX = 0.99;
    static final double shrinkFactorY = 0.99;
    static final int numberOfStepsTillRecede = 50;
    static double upperStep,lowerStep;
    boolean visible = false;
    Timeline timeline;
    Timeline timeline2;
    String versed="";
    
    private static final String propFileName = "ass2.properties";

    // TODO crawlFileName must be chosen using the menu "Episode"
    // TODO after the application has started (part of Task 2)

    private static final String crawlFileName = "StarWars-1";
    private static final String crawlFileName1 = "StarWars-2";
    private static final String crawlFileName2 = "StarWars-3";
    private static final String crawlFileName3 = "StarWars-4";
    private static final String crawlFileName4 = "StarWars-5";
    private static final String crawlFileName5 = "StarWars-6";

    public static void main(String[] args)
            throws IOException {

        Application.launch(args);
    }


    /** override start method which creates the scene
     *  and all nodes and shapes in it (main window only),
     *  and redefines how the nodes react to user inputs
     *  and other events;
     *  @param primaryStage Stage (the top level container)
     *  */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Star Wars");

//        Parameters parameters = getParameters(); // for picking args
        Properties props = new Properties(System.getProperties());
        try {
            props.load(new FileReader(new File(propFileName)));
            System.setProperties(props);
        } catch (IOException e) {
            System.out.printf("properties file cannot be read%n");
            System.exit(1);
        }

        String fontFileName = System.getProperty(crawlFileName+versed);
        System.out.println(fontFileName);

        try {
            model = new CrawlReader(fontFileName);
        } catch (IOException e) {
            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }
        

        /* the main window */
        root = new Group();
        // TODO replace the previous statement with the try-catch block below
        // TODO if implement the layout via FXML (using CrawlView.fxml and Controller
		// IMPORTANT: this is not required by the assignment specification!
//        try {
//            root = FXMLLoader.load(getClass().getResource("view/CrawlView.fxml"));
//        } catch (IOException e) {
//            System.out.println(e);
//            System.exit(2);
//        }
         
        final Scene scene = new Scene(root,700, 500, Color.BLACK);
        
        
        
        double centreX = scene.getX() + 0.5 * scene.getWidth();
        double centreY = scene.getY() + 0.5 * scene.getHeight();
        double bottom = centreY + 0.5*scene.getHeight();
        upperStep = - scene.getHeight() / numberOfStepsTillRecede;
        lowerStep =  scene.getHeight() / numberOfStepsTillRecede;
        double top = scene.getY();
        System.out.println("Parameters of the main window:");
        System.out.printf("  width -- %.0f%n  height -- %.0f%n centre -- (%.0f,%.0f)%n",
                scene.getWidth(), scene.getHeight(), centreX, centreY);

        Text text = new Text();
        Text text2 = new Text();
        //if(!startTransition){
        text2.setText("A long time ago, in a galaxy far, far away...");
        text2.setTextAlignment(TextAlignment.CENTER);
        text2.setFont(new Font("Verdana Bold", 30));
        text2.setWrappingWidth(700);
        text2.setLayoutY(top+500/2);
        text2.setFill(Color.BEIGE);
        root.getChildren().add(text2);     
                
        FadeTransition ft = new FadeTransition(Duration.millis(3000), text2);
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.play();
        //startTransition = true;
        //}           

        //if(startTransition){
        String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
        
        //FadeTransition ft1 = new FadeTransition(Duration.millis(3000), text);
        //ft1.setFromValue(2.0);
        //ft1.setToValue(0.0);
        //ft1.play();

        // rotation angle must be adjusted (reduced) at every step to make
        // the effect of receding view more realistic; right now, it's flawed;
        // TODO if you decide to exclude turning effect remove the next line
        // TODO to implement it properly, the rotation angle (1st arg in Rotate)
        // TODO must be replaced by a method call to make turning angle decrease
        // TODO at every time step (part of Task "bonus")
        group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        
       //GridPane gridpane = new GridPane();
        
        Button episodes = new Button("Episodes");
        episodes.setLayoutX(0);
        episodes.setMnemonicParsing(false);
        root.getChildren().add(episodes); 
       
        
        Button episode1 = new Button("Episode1");
        episode1.setLayoutY(30);
     
        episode1.setMnemonicParsing(false);
        episode1.setVisible(visible);      
        root.getChildren().add(episode1); 
        
        Button episode2 = new Button("Episode2");
        episode2.setLayoutY(60);
        episode2.setMnemonicParsing(false);
        episode2.setVisible(visible);
        root.getChildren().add(episode2); 
        
        Button episode3 = new Button("Episode3");
        episode3.setLayoutY(90);
        episode3.setMnemonicParsing(false);
        episode3.setVisible(visible);
        root.getChildren().add(episode3); 
        
        Button episode4 = new Button("Episode4");
        episode4.setLayoutY(120);
        episode4.setMnemonicParsing(false);
        episode4.setVisible(visible);
        root.getChildren().add(episode4); 
        
        Button episode5 = new Button("Episode5");
        episode5.setLayoutY(150);
        episode5.setMnemonicParsing(false);
        episode5.setVisible(visible);
        root.getChildren().add(episode5);
        
        Button episode6 = new Button("Episode6");
        episode6.setLayoutY(180);
        episode6.setMnemonicParsing(false);
        episode6.setVisible(visible);
        root.getChildren().add(episode6); 
         episodes.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
        visible = true;
        
        episode1.setVisible(visible);
        episode2.setVisible(visible);
        episode3.setVisible(visible);
        episode4.setVisible(visible);
        episode5.setVisible(visible);
        episode6.setVisible(visible);
        
           
       }                
});
         episode1.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
        
        String fontFileName = System.getProperty(crawlFileName+versed);
        System.out.println(fontFileName);

        try {
            model = new CrawlReader(fontFileName);
        } catch (IOException e1) {
            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }
        
        String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
        
                timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        visible = false;
        
        episode1.setVisible(visible);
        episode2.setVisible(visible);
        episode3.setVisible(visible);
        episode4.setVisible(visible);
        episode5.setVisible(visible);
        episode6.setVisible(visible);
        
           
       }                
});
         
          episode2.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
        
        String fontFileName = System.getProperty(crawlFileName1+versed);
        System.out.println(fontFileName);

        try {
            model = new CrawlReader(fontFileName);
        } catch (IOException e1) {
            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }
        
        String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
        
                timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        visible = false;
        
        episode1.setVisible(visible);
        episode2.setVisible(visible);
        episode3.setVisible(visible);
        episode4.setVisible(visible);
        episode5.setVisible(visible);
        episode6.setVisible(visible);
        
           
       }                
});
          episode3.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
        
        String fontFileName = System.getProperty(crawlFileName2+versed);
        System.out.println(fontFileName);

        try {
            model = new CrawlReader(fontFileName);
        } catch (IOException e1) {
            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }
        
        String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
        
                timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        visible = false;
        
        episode1.setVisible(visible);
        episode2.setVisible(visible);
        episode3.setVisible(visible);
        episode4.setVisible(visible);
        episode5.setVisible(visible);
        episode6.setVisible(visible);
        
           
       }                
});
          episode4.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
        
        String fontFileName = System.getProperty(crawlFileName3+versed);
        System.out.println(fontFileName);

        try {
            model = new CrawlReader(fontFileName);
        } catch (IOException e1) {
            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }
        
        String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
        
               timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        visible = false;
        
        episode1.setVisible(visible);
        episode2.setVisible(visible);
        episode3.setVisible(visible);
        episode4.setVisible(visible);
        episode5.setVisible(visible);
        episode6.setVisible(visible);
        
           
       }                
});
          episode5.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
        
        String fontFileName = System.getProperty(crawlFileName4+versed);
        System.out.println(fontFileName);

        try {
            model = new CrawlReader(fontFileName);
        } catch (IOException e1) {
            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }
        
        String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
        
               timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        visible = false;
        
        episode1.setVisible(visible);
        episode2.setVisible(visible);
        episode3.setVisible(visible);
        episode4.setVisible(visible);
        episode5.setVisible(visible);
        episode6.setVisible(visible);
        
           
       }                
});
          episode6.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
        
        String fontFileName = System.getProperty(crawlFileName5+versed);
        System.out.println(fontFileName);

        try {
            model = new CrawlReader(fontFileName);
        } catch (IOException e1) {
            System.err.printf("cannot open the file %s%n", fontFileName);
            System.exit(2);
        }
        
        String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
        
                timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        visible = false;
        
        episode1.setVisible(visible);
        episode2.setVisible(visible);
        episode3.setVisible(visible);
        episode4.setVisible(visible);
        episode5.setVisible(visible);
        episode6.setVisible(visible);
        
           
       }                
});
        Button play = new Button("Play");
        play.setLayoutX(75);
        play.setMnemonicParsing(false);
        root.getChildren().add(play); 
         timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
         timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        play.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
    timeline.setCycleCount(Timeline.INDEFINITE);
          //timeline.getKeyFrames().add();
          timeline.play();         
       }
                  
  
});
        
       Button pause = new Button("Pause");
       pause.setLayoutX(150);
        pause.setMnemonicParsing(false);
        root.getChildren().add(pause); 
          pause.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
          timeline2.pause();
          timeline.pause();         
       }
                  
  
});
        Button reverse = new Button("Reverse");
        reverse.setLayoutX(225);
        reverse.setMnemonicParsing(false);
        root.getChildren().add(reverse); 
        reverse.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
    
           timeline2.setCycleCount(Timeline.INDEFINITE);
          //timeline.getKeyFrames().add();
          timeline2.play(); 
          
       }
                  
  
});
        Button clear = new Button("Clear");
        clear.setLayoutX(300);
        clear.setMnemonicParsing(false);
        root.getChildren().add(clear);
        CheckBox master = new CheckBox("Master");
        master.setLayoutX(375);
        master.setTextFill(Color.web("#FFFFFF"));
   
        root.getChildren().add(master);
        master.selectedProperty().addListener(new ChangeListener<Boolean>() {
           public void changed(ObservableValue<? extends Boolean> ov,
             Boolean old_val, Boolean new_val) {
             if(master.isSelected())
             {
                versed="_nonversed"; 
             }else{
                 versed="";
             }
          }
        });
        clear.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       String Verses="";
        model.makeVerses(41,2,2);
        for(String lines:model.makeVersedLines()){
            Verses=Verses+lines+"\n";
        }
        String beginning = String.format("%s%n%s%n%s%n%s",
                model.getTitle(),
                model.getSubTitle(),
                model.getPrefix(),
                Verses);
        text.setText(beginning);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setFont(new Font("Verdana Bold", 30));
        text.setFill(Color.GOLDENROD);
        Group group = new Group();
        group.getChildren().addAll(text);
        group.setLayoutX(centreX - 0.5*group.getLayoutBounds().getWidth());
        group.setLayoutY(bottom);
       group.getTransforms().add(new Rotate(5, new Point3D(1,0,0)));
        root.getChildren().addAll(group);
        timeline = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
        timeline2 = new Timeline(
      new KeyFrame(Duration.ZERO, new EventHandler() {
        @Override public void handle(Event event) {
      
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, lowerStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.0, new Point3D(1,0,0)));
                    
                    
                     }
      }),  
      new KeyFrame(Duration.millis(100))
    );
//        if(){}
         
           
          
       }
                  
  
});
        play.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
    timeline.setCycleCount(Timeline.INDEFINITE);
          //timeline.getKeyFrames().add();
          timeline.play();         
       }
                  
  
});
         pause.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
          timeline2.pause();
          timeline.pause();         
       }
                  
  
});
         reverse.setOnAction(new EventHandler<ActionEvent>() {
    @Override public void handle(ActionEvent e) {
       
    
           timeline2.setCycleCount(Timeline.INDEFINITE);
          //timeline.getKeyFrames().add();
          timeline2.play(); 
          
       }
                  
  
});
         
        // define the key pressed callback to generate flying text
        // TODO students will have to remove this, and introduce
        // TODO transitions instead (part of Task 1)
       /* scene.onKeyPressedProperty().set(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent ke) {

                if (ke.getCode() == KeyCode.UP) {
                    //while(true){
                    double w,h;
                    w = group.getLayoutBounds().getWidth();
                    h = 0.5 * (1 - shrinkFactorX) * w;
                   
                    shiftAndScale(group, h, upperStep, shrinkFactorX, shrinkFactorY);
                    group.getTransforms().add(new Rotate(0.75, new Point3D(1,0,0)));
                    return;
                    //}
                }
            }
        });*/
        
        
        
        // finalise and display the whole scene graph
        primaryStage.setScene(scene);
        //Star
        Group circles = new Group();
        for(int i=0;i<60;i++){
            Circle circle = new Circle(0.7, Color.web("white"));
            circle.setStrokeType(StrokeType.INSIDE);
            circle.setStroke(Color.web("white"));
            circle.setStrokeWidth(0.5);
          
            circle.setCenterX(random()*700);
            circle.setCenterY(random()*500);
            circles.getChildren().add(circle);
        }
        root.getChildren().add(circles);
        
        
        primaryStage.show();
        
        
    }

    /** an auxiliary method to reduce and move a node (which may contain a text line)
	  * as as result one one time-step animation
	  * @param node is the node to shift and scale
	  * @param shiftX horizontal shift
	  * @param shiftY vertical shift (negative values mean upwards)
	  * @param scaleX horizontal shrikage coefficient
	  * @param scaleY vertical shrikage
      */
	private void shiftAndScale(Node node,
                                double shiftX, double shiftY,
                                double scaleX, double scaleY) {
        Translate t = new Translate();
        Scale s = new Scale();
        t.setX(shiftX);
        t.setY(shiftY);
        s.setX(scaleX);
        s.setY(scaleY);
        node.getTransforms().addAll(t,s);
    }
        

}
